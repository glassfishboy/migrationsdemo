// <auto-generated />
namespace MigrationsDemo.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class AddStudentClass : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddStudentClass));
        
        string IMigrationMetadata.Id
        {
            get { return "201401030835488_AddStudentClass"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
