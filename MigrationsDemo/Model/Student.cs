﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MigrationsDemo
{
    public class Student
    {
        [Key]
        public int ID { get; set; }
        public string name { get; set; }
    }
}

